<?php include 'header.php' ?>
<div class="how-help">
    <div class="container">
        <div class="how-help-title" data-aos="zoom-in" data-aos-duration='1200'>
            <h1>Банковские карты</h1>
        </div>
        <div class="how-help-inner">
            <p data-aos="zoom-in" data-aos-duration='1200'>Пожертвования для</p>
            <div class="how-help-select" data-aos="zoom-in" data-aos-duration='1500'>
                <select class="help-select" name="state">
                    <option  selected>Выберите из списка...</option>
                    <option >Хирурга Михаила Куратовича</option>
                    <option >Консультация врача ортопеда</option>
                    <option >Саминов Артем</option>
                    <option >Карикарович Масикан</option>
                    <option >Кундашев Анатолий</option>
                    <option >Невропотолог Стас</option>
                    <option >Колина Мария</option>
                  </select>
            </div>
            <div class="how-help-qr" data-aos="zoom-in" data-aos-duration='1500'>
                <p>Отсканируйте QR с помощью телефона</p>
                <img src="images/qr.png" alt="">
                <a href="">+7 777 555 12 50</a>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php' ?>