<?php include 'header.php' ?>
<div class="page-wrapper">
    <div class="content-wrapper">
        <main class="main">
            <div class="banner-wrapper">
                <div id="banner" class="banner owl-carousel owl-theme">
                    <div class="banner-slide" style="background: url(images/banner.jpg)">
                        <div class="banner-inner container">
                            <div class="banner-caption">
                                <h3>
                                    Помогите <strong>спасти жизнь</strong>
                                    ребенку в несколько
                                    кликов
                                </h3>
                                <p>Вы можете внести пожертвование на любую сумму,
                                    тем самым спасете чью-то жизнь</p>
                                <button class="banner-button" type="button">
                                    <strong>Подробнее</strong>
                                    <strong>
                                        <svg viewBox="0 0 16 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M3.26564 0.596608L15.4397 13.5597C16.1868 14.3551 16.1868 15.6449 15.4397 16.4403L3.26564 29.4034C2.51858 30.1989 1.30736 30.1989 0.560296 29.4034C-0.186765 28.6079 -0.186765 27.3182 0.560296 26.5227L11.3817 15L0.560296 3.47729C-0.186765 2.68181 -0.186765 1.39209 0.560296 0.596608C1.30736 -0.198869 2.51858 -0.198869 3.26564 0.596608Z"
                                                fill="#1C1C1C" />
                                        </svg>
                                    </strong>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="banner-slide" style="background: url(images/banner.jpg)">
                        <div class="banner-inner container">
                            <div class="banner-caption">
                                <h3>
                                    Помогите <strong>спасти жизнь</strong>
                                    ребенку в несколько
                                    кликов
                                </h3>
                                <p>Вы можете внести пожертвование на любую сумму,
                                    тем самым спасете чью-то жизнь</p>
                                <button class="banner-button" type="button">
                                    <strong>Подробнее</strong>
                                    <strong>
                                        <svg viewBox="0 0 16 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M3.26564 0.596608L15.4397 13.5597C16.1868 14.3551 16.1868 15.6449 15.4397 16.4403L3.26564 29.4034C2.51858 30.1989 1.30736 30.1989 0.560296 29.4034C-0.186765 28.6079 -0.186765 27.3182 0.560296 26.5227L11.3817 15L0.560296 3.47729C-0.186765 2.68181 -0.186765 1.39209 0.560296 0.596608C1.30736 -0.198869 2.51858 -0.198869 3.26564 0.596608Z"
                                                fill="#1C1C1C" />
                                        </svg>
                                    </strong>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="banner-slide" style="background: url(images/banner.jpg)">
                        <div class="banner-inner container">
                            <div class="banner-caption">
                                <h3>
                                    Помогите <strong>спасти жизнь</strong>
                                    ребенку в несколько
                                    кликов
                                </h3>
                                <p>Вы можете внести пожертвование на любую сумму,
                                    тем самым спасете чью-то жизнь</p>
                                <button class="banner-button" type="button">
                                    <strong>Подробнее</strong>
                                    <strong>
                                        <svg viewBox="0 0 16 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M3.26564 0.596608L15.4397 13.5597C16.1868 14.3551 16.1868 15.6449 15.4397 16.4403L3.26564 29.4034C2.51858 30.1989 1.30736 30.1989 0.560296 29.4034C-0.186765 28.6079 -0.186765 27.3182 0.560296 26.5227L11.3817 15L0.560296 3.47729C-0.186765 2.68181 -0.186765 1.39209 0.560296 0.596608C1.30736 -0.198869 2.51858 -0.198869 3.26564 0.596608Z"
                                                fill="#1C1C1C" />
                                        </svg>
                                    </strong>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dots-wrapper container">
                    <ul id="carousel-custom-dots" class="owl-dots">
                        <li class="active owl-dot"></li>
                        <li class="owl-dot"></li>
                        <li class="owl-dot"></li>
                    </ul>
                </div>
            </div>

            <div class="help-block">
                <div class="send-help-wrapper container">
                    <div class="send-help" data-aos-offset="200" data-aos="fade-up" data-aos-duration="1000"
                        data-aos-delay="100">
                        <h4>Отправить помощь</h4>
                        
                        <div class="send-help-row">
                            <div class="price-wrap">
                                <button type="button" class="price price-info">250 руб</button>
                                <button type="button" class="price price-info">500 руб</button>
                                <button type="button" class="price price-info">1000 руб</button>
                                <input type="number" class="other-input price-info" placeholder="Другая сумма">
                            </div>
                            <button class="want-help-button" type="button" disabled>Хочу помочь</button>
                        </div>
                    </div>
                </div>

                <h4 class="main-page-title container">Помощь детям</h4>

                <div class="help-children">
                    <div class="children-carousel-wrapper container">
                        <button id="nav-prev" class="children-carousel-nav prev" type="button">
                            <img src="images/nav-prev.svg" alt="Prev">
                        </button>
                        <div id="help-children" class="children-carousel owl-carousel owl-theme" data-aos-offset="200"
                            data-aos="fade-up" data-aos-duration="1000" data-aos-delay="100">
                            <div class="child-card">
                                <div class="child-card-img">
                                    <img src="images/danila.jpg" alt="Child img">
                                </div>
                                <div class="child-info">
                                    <p class="name">Шестаков Данила</p>
                                    <div class="status-bar">
                                        <strong class="occupancy" style="width: 40%;"></strong>
                                    </div>
                                    <p class="raising-money">
                                        Осталось собрать <strong>118 000 руб</strong>
                                    </p>
                                </div>
                                <div class="card-dropdown">
                                    <button class="card-button" type="button">Хочу помочь</button>
                                    <button class="card-button" type="button">Читать историю</button>
                                </div>
                            </div>
                            <div class="child-card">
                                <div class="child-card-img">
                                    <img src="images/masha.jpg" alt="Child img">
                                </div>
                                <div class="child-info">
                                    <p class="name">Кузнецова Анна</p>
                                    <div class="status-bar">
                                        <strong class="occupancy" style="width: 30%;"></strong>
                                    </div>
                                    <p class="raising-money">
                                        Осталось собрать <strong>250 000 руб</strong>
                                    </p>
                                </div>
                                <div class="card-dropdown">
                                    <button class="card-button" type="button">Хочу помочь</button>
                                    <button class="card-button" type="button">Читать историю</button>
                                </div>
                            </div>
                            <div class="child-card">
                                <div class="child-card-img">
                                    <img src="images/ivan.jpg" alt="Child img">
                                </div>
                                <div class="child-info">
                                    <p class="name">Точилин Иван</p>
                                    <div class="status-bar">
                                        <strong class="occupancy" style="width: 85%;"></strong>
                                    </div>
                                    <p class="raising-money">
                                        Осталось собрать <strong>12 500 руб</strong>
                                    </p>
                                </div>
                                <div class="card-dropdown">
                                    <button class="card-button" type="button">Хочу помочь</button>
                                    <button class="card-button" type="button">Читать историю</button>
                                </div>
                            </div>
                            <div class="child-card">
                                <div class="child-card-img">
                                    <img src="images/Yana.jpg" alt="Child img">
                                </div>
                                <div class="child-info">
                                    <p class="name">Жлезнякова Яна</p>
                                    <div class="status-bar">
                                        <strong class="occupancy" style="width: 60%;"></strong>
                                    </div>
                                    <p class="raising-money">
                                        Осталось собрать <strong>118 000 руб</strong>
                                    </p>
                                </div>
                                <div class="card-dropdown">
                                    <button class="card-button" type="button">Хочу помочь</button>
                                    <button class="card-button" type="button">Читать историю</button>
                                </div>
                            </div>
                        </div>
                        <button id="nav-next" class="children-carousel-nav next" type="button">
                            <img src="images/nav-next.svg" alt="Next">
                        </button>
                    </div>

                    <a class="watch-all container" href="#">Смореть всех, кто нуждается <img
                            src="images/link-arrow.png" alt="Link arrow"></a>

                    <img class="child-sox" src="images/child-sox.jpg" alt="Помощь детям">
                </div>
            </div>

            <h4 class="main-page-title container">Проекты</h4>
            <div class="projects" data-aos-offset="200" data-aos="fade-up" data-aos-duration="1000"
                data-aos-delay="100">
                <div class="project-card" style="background: url(images/project1.jpg);">
                    <p>Проект „Charity Box“</p>
                    <a href="#">
                        <svg viewBox="0 0 16 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M3.26564 0.596608L15.4397 13.5597C16.1868 14.3551 16.1868 15.6449 15.4397 16.4403L3.26564 29.4034C2.51858 30.1989 1.30736 30.1989 0.560296 29.4034C-0.186765 28.6079 -0.186765 27.3182 0.560296 26.5227L11.3817 15L0.560296 3.47729C-0.186765 2.68181 -0.186765 1.39209 0.560296 0.596608C1.30736 -0.198869 2.51858 -0.198869 3.26564 0.596608Z"
                                fill="#1C1C1C" />
                        </svg>
                    </a>
                </div>
                <div class="project-card" style="background: url(images/project2.jpg);">
                    <p>Проект „Помощи детям с ДЦП в лечении и реабилитации“</p>
                    <a href="#">
                        <svg viewBox="0 0 16 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M3.26564 0.596608L15.4397 13.5597C16.1868 14.3551 16.1868 15.6449 15.4397 16.4403L3.26564 29.4034C2.51858 30.1989 1.30736 30.1989 0.560296 29.4034C-0.186765 28.6079 -0.186765 27.3182 0.560296 26.5227L11.3817 15L0.560296 3.47729C-0.186765 2.68181 -0.186765 1.39209 0.560296 0.596608C1.30736 -0.198869 2.51858 -0.198869 3.26564 0.596608Z"
                                fill="#1C1C1C" />
                        </svg>
                    </a>
                </div>
                <div class="project-card" style="background: url(images/project3.jpg);">
                    <p>Проект „Помощь учреждениям и организациям“</p>
                    <a href="#">
                        <svg viewBox="0 0 16 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M3.26564 0.596608L15.4397 13.5597C16.1868 14.3551 16.1868 15.6449 15.4397 16.4403L3.26564 29.4034C2.51858 30.1989 1.30736 30.1989 0.560296 29.4034C-0.186765 28.6079 -0.186765 27.3182 0.560296 26.5227L11.3817 15L0.560296 3.47729C-0.186765 2.68181 -0.186765 1.39209 0.560296 0.596608C1.30736 -0.198869 2.51858 -0.198869 3.26564 0.596608Z"
                                fill="#1C1C1C" />
                        </svg>
                    </a>
                </div>
            </div>

            <a class="all-project container" href="project.php">Смореть все проекты <img src="images/link-arrow.png"
                    alt="Link arrow"></a>

            <h4 class="main-page-title container">Новости</h4>

            <div class="news container" data-aos-offset="200" data-aos="fade-up" data-aos-duration="1000"
                data-aos-delay="100">
                <div class="news-card">
                    <a href="project-inner.php" class="news-card-img">
                        <img src="images/news1.jpg" alt="">
                    </a>
                    <p class="date">12.10.2020</p>
                    <a href="project-inner.php" class="content">Покупка АйТрекера для Дениса
                        Бартоша - результаты работы</a>
                    <p class="news__card-content">Минтруд России сообщил об издании приказа, по которому упрощается порядок замены некоторых......</p>
                </div>
                <div class="news-card">
                    <a href="project-inner.php" class="news-card-img">
                        <img src="images/news2.jpg" alt="">
                    </a>
                    <p class="date">12.10.2020</p>
                    <a href="project-inner.php" class="content">Ремонт и списание ТСР</a>
                    <p class="news__card-content">Минтруд России сообщил об издании приказа, по которому упрощается порядок замены некоторых......</p>
                </div>
                <div class="news-card">
                    <a href="project-inner.php" class="news-card-img">
                        <img src="images/news3.jpg" alt="">
                    </a>
                    <p class="date">12.10.2020</p>
                    <a href="project-inner.php" class="content">Поможем Денису продолжить занятия
                        на лошади</a>
                    <p class="news__card-content">Минтруд России сообщил об издании приказа, по которому упрощается порядок замены некоторых......</p>
                </div>
            </div>

            <a class="watch-all container news-all" href="news.php">Смореть все новости <img
                    src="images/link-arrow.png" alt="Link arrow"></a>

            <div class="cooperation">
                <div class="cooperation-inner container">
                    <h3 data-aos-offset="200" data-aos="fade-up-right" data-aos-duration="1000" data-aos-delay="100">
                        Интересует сотрудничество с нашим фондом?</h3>
                    <p data-aos-offset="200" data-aos="fade-up-right" data-aos-duration="1000" data-aos-delay="100">
                        Оставьте заявку и мы проконсультируем вас по любому вопросу</p>
                    <form action="index.php" method="POST">
                        <div class="input-field" data-aos-offset="200" data-aos="fade-up-right" data-aos-duration="1000"
                            data-aos-delay="100">
                            <strong>Ваше имя</strong>
                            <input type="text" placeholder="Жасулан">
                        </div>
                        <div class="input-field" data-aos-offset="200" data-aos="fade-up-right" data-aos-duration="1000"
                            data-aos-delay="100">
                            <strong>Ваш номер</strong>
                            <input type="tel" placeholder="+7(">
                        </div>
                        <button class="banner-button cooperation-btn" type="button" data-aos-offset="200"
                            data-aos="fade-up-right" data-aos-duration="1000" data-aos-delay="100">
                            <strong>Оставить заявку</strong>
                        </button>
                    </form>
                </div>
            </div>

            <h4 class="main-page-title container">Вы уже помогли</h4>

            <div class="already-helped container" data-aos-offset="200" data-aos="fade-up" data-aos-duration="1000"
                data-aos-delay="100">
                <div class="helped-card">
                    <a href="#" class="helped-card-img">
                        <img src="images/helped1.jpg" alt="Already helped">
                    </a>
                    <a href="#" class="helped-card-link">
                        <p class="name">Айдамиров Миша</p>
                        <p class="info">18 лет, Нур-Султан</p>
                        <svg viewBox="0 0 16 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M3.26564 0.596608L15.4397 13.5597C16.1868 14.3551 16.1868 15.6449 15.4397 16.4403L3.26564 29.4034C2.51858 30.1989 1.30736 30.1989 0.560296 29.4034C-0.186765 28.6079 -0.186765 27.3182 0.560296 26.5227L11.3817 15L0.560296 3.47729C-0.186765 2.68181 -0.186765 1.39209 0.560296 0.596608C1.30736 -0.198869 2.51858 -0.198869 3.26564 0.596608Z"
                                fill="#1C1C1C" />
                        </svg>
                    </a>
                </div>

                <div class="helped-card">
                    <a href="#" class="helped-card-img">
                        <img src="images/helped2.jpg" alt="Already helped">
                    </a>
                    <a href="#" class="helped-card-link">
                        <p class="name">Айдамиров Миша</p>
                        <p class="info">18 лет, Нур-Султан</p>
                        <svg viewBox="0 0 16 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M3.26564 0.596608L15.4397 13.5597C16.1868 14.3551 16.1868 15.6449 15.4397 16.4403L3.26564 29.4034C2.51858 30.1989 1.30736 30.1989 0.560296 29.4034C-0.186765 28.6079 -0.186765 27.3182 0.560296 26.5227L11.3817 15L0.560296 3.47729C-0.186765 2.68181 -0.186765 1.39209 0.560296 0.596608C1.30736 -0.198869 2.51858 -0.198869 3.26564 0.596608Z"
                                fill="#1C1C1C" />
                        </svg>
                    </a>
                </div>

                <div class="helped-card">
                    <a href="#" class="helped-card-img">
                        <img src="images/helped3.jpg" alt="Already helped">
                    </a>
                    <a href="#" class="helped-card-link">
                        <p class="name">Айдамиров Миша</p>
                        <p class="info">18 лет, Нур-Султан</p>
                        <svg viewBox="0 0 16 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M3.26564 0.596608L15.4397 13.5597C16.1868 14.3551 16.1868 15.6449 15.4397 16.4403L3.26564 29.4034C2.51858 30.1989 1.30736 30.1989 0.560296 29.4034C-0.186765 28.6079 -0.186765 27.3182 0.560296 26.5227L11.3817 15L0.560296 3.47729C-0.186765 2.68181 -0.186765 1.39209 0.560296 0.596608C1.30736 -0.198869 2.51858 -0.198869 3.26564 0.596608Z"
                                fill="#1C1C1C" />
                        </svg>
                    </a>
                </div>
            </div>

            <a class="watch-all container helped-all" href="#">Смореть все истории <img
                    src="images/link-arrow.png" alt="Link arrow"></a>
        </main>
        <!-- end main -->
    </div>
</div>
<?php include 'footer.php' ?>