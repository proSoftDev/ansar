<?php include 'header.php' ?>
<div class="page-wrapper">
    <div class="content-wrapper">
        <main class="main">
            <div class="main__wrapper container">
                <h4 class="page-title left">Контакты</h4>

                <div class="contacts__row">
                    <div class="contacts__left" data-aos-offset="200" data-aos="fade-right" data-aos-duration="1000"
                        data-aos-delay="100">
                        <p class="title">
                            Напишите нам
                        </p>
                        <form class="contacts__form" action="contacts.html" method="POST">
                            <input type="text" placeholder="Имя">
                            <input type="email" placeholder="Почта">
                            <input type="tel" placeholder="Телефон">
                            <textarea name="message" placeholder="Сообщение"></textarea>
                            <button class="contacts__button" type="button">Отправить</button>
                        </form>
                    </div>

                    <div class="contacts__right" data-aos-offset="200" data-aos="fade-left" data-aos-duration="1000"
                        data-aos-delay="100">
                        <div class="map">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2444.0231265868456!2d76.9391040823608!3d43.241996315799724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38836ec2b07a60bf%3A0x34eca90f5c848460!2sAbaya%20-%20Zheltoksan!5e0!3m2!1skk!2skz!4v1593417579691!5m2!1skk!2skz"
                                frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
                                tabindex="0"></iframe>
                        </div>

                        <div class="info">
                            <div class="info__left">
                                <p>127006, Алматы<br> ул. Красная, д. 16, стр. 6</p>
                            </div>
                            <div class="info__right">
                                <a href="tel: +7(499)500-14-15">+7 (499) 500-14-15,</a>
                                <a href="mailto: info@life-line.ru">Info@life-line.ru</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
<?php include 'footer.php' ?>