<?php include 'header.php' ?>
<div class="fund-page content">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная </a></li>
                <li class="breadcrumb-item active" aria-current="page">О фонде</li>
            </ol>
        </nav>
        <div class="title">
            <h1>О фонде</h1>
        </div>
        <h2>Благотворительный фонд <strong>«ДОБРОСЕРДИЕ»</strong> — российская некоммерческая организация, которая оказывает
            помощь в лечении и реабилитации детей <strong>с диагнозом детский церебральный паралич (ДЦП).</strong></h2>
        <div class="title">
            <h3>Деятельность фонда</h3>
            <div class="fund-list">
                <h2 class="fund-subtitle">Благополучатели фонда:</h2>
                <ul>
                    <li>Дети- инвалиды с заболеванием церебральный паралич (ДЦП), проживающие в Москве и Московской
                        области.</li>
                    <li>Законные представители детей с ДЦП.</li>
                    <li>Малообеспеченные семьи, воспитывающие одного или более детей с ДЦП</li>
                    <li>Некоммерческие медицинские учреждения и организации на территории Москвы и Московской области, а
                        также специалистыб занимающиеся помощью детям с ДЦП.</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="about-fund">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <div class="fund-wrapper">
                        <h2 class="fund-subtitle">Миссия</h2>
                        <p>Системное оказание помощи детям с детским церебральным параличом (ДЦП) в процессе лечения,
                            реабилитации, социализации, адаптации и интеграции в общество, поддержка их семей.</p>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="fund-wrapper">
                        <h2 class="fund-subtitle">Видение фонда:</h2>
                        <p>Будущее фонда мы видим в развитии новых форм благотворительности, в интеграции понятия
                            «благотворительность» в сознание людей.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fund-values">
        <div class="container">
            <h2 class="fund-subtitle">Ценности фонда:</h2>
            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <div class="fund-values-item">
                        <strong>Профессионализм и компетентность</strong>
                        <p>Наша команда состоит из специалистов, обладающих необходимыми знаниями и опытом, любящих свою
                            работу. Это позволяет нам эффективно решать поставленные задачи и добиваться лучших
                            результатов.</p>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="fund-values-item">
                        <strong>Прозрачность</strong>
                        <p>Мы придерживаемся ясности, основанной на доступности информации. Мы регулярно и своевременно
                            публикуем финансовые и другие отчеты по нашей деятельности.</p>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="fund-values-item">
                        <strong>Ответственность</strong>
                        <p>Мы работаем во благо ребенка с ограниченными возможностями здоровья, понимая степень
                            ответственности, возложенную на нас, в рамках программ
                            и проектов Фонда. Мы делаем все от нас зависящее для достижения поставленных целей и задач.
                        </p>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="fund-values-item">
                        <strong>Открытость</strong>
                        <p>Мы открыты к сотрудничеству в интересах наших благополучателей. Мы ценим и уважаем всех наших
                            партнеров и стремимся сделать наше взаимодействие максимально комфортным и продуктивным.
                        </p>
                    </div>
                </div>
            </div>
            <div class="title">
                <h3>Команда</h3>
            </div>
            <div class="team-slider">
                <div class="team-slide">
                    <div class="team-image">
                        <img src="images/team.png" alt="">
                    </div>
                    <div class="team-info">
                    <h2>Надежда Корсакова </h2>    
                    <h1>Президент  благотворительного  фонда <strong>«Ансар»</strong></h1>
                        <a href="">pr@ansar.com</a>
                    </div>
                </div>
                <div class="team-slide">
                    <div class="team-image">
                        <img src="images/team.png" alt="">
                    </div>
                    <div class="team-info">
                    <h2>Надежда Корсакова </h2>    
                    <h1>Президент  благотворительного  фонда <strong>«Ансар»</strong></h1>
                        <a href="">pr@ansar.com</a>
                    </div>
                </div>
                <div class="team-slide">
                    <div class="team-image">
                        <img src="images/team.png" alt="">
                    </div>
                    <div class="team-info">
                    <h2>Надежда Корсакова </h2>    
                    <h1>Президент  благотворительного  фонда <strong>«Ансар»</strong></h1>
                        <a href="">pr@ansar.com</a>
                    </div>
                </div>
                <div class="team-slide">
                    <div class="team-image">
                        <img src="images/team.png" alt="">
                    </div>
                    <div class="team-info">
                    <h2>Надежда Корсакова </h2>    
                    <h1>Президент  благотворительного  фонда <strong>«Ансар»</strong></h1>
                        <a href="">pr@ansar.com</a>
                    </div>
                </div>
                <div class="team-slide">
                    <div class="team-image">
                        <img src="images/team.png" alt="">
                    </div>
                    <div class="team-info">
                        <h1>Президент  благотворительного  фонда <strong>«Ансар»</strong></h1>
                        <a href="">pr@ansar.com</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="fund-partners">
        <div class="container">
            <div class="title">
                <h3>Партнеры</h3>
            </div>
            <div class="partners-slider">
                <div class="partner-slide">
                    <img src="images/partner1.png" alt="">
                </div>
                <div class="partner-slide">
                    <img src="images/partner2.png" alt="">
                </div>
                <div class="partner-slide">
                    <img src="images/partner3.png" alt="">
                </div>
                <div class="partner-slide">
                    <img src="images/partner4.png" alt="">
                </div>
                <div class="partner-slide">
                    <img src="images/partner1.png" alt="">
                </div>
                <div class="partner-slide">
                    <img src="images/partner1.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="about-media">
        <div class="container">
            <div class="title">
                <h3>Сми о нас</h3>
            </div>
            <div class="media-slider">
                <div class="media-slide">
                    <div class="media-image">
                        <img src="images/media.png" alt="">
                    </div>
                    <div class="media-text">
                        <strong>12.10.2020</strong>
                        <h1>Покупка АйТрекера для Дениса
                            Бартоша - результаты работы</h1>
                    </div>
                </div>
                <div class="media-slide">
                    <div class="media-image">
                        <img src="images/media.png" alt="">
                    </div>
                    <div class="media-text">
                        <strong>12.10.2020</strong>
                        <h1>Покупка АйТрекера для Дениса
                            Бартоша - результаты работы</h1>
                    </div>
                </div>
                <div class="media-slide">
                    <div class="media-image">
                        <img src="images/media.png" alt="">
                    </div>
                    <div class="media-text">
                        <strong>12.10.2020</strong>
                        <h1>Покупка АйТрекера для Дениса
                            Бартоша - результаты работы</h1>
                    </div>
                </div>
                <div class="media-slide">
                    <div class="media-image">
                        <img src="images/media.png" alt="">
                    </div>
                    <div class="media-text">
                        <strong>12.10.2020</strong>
                        <h1>Покупка АйТрекера для Дениса
                            Бартоша - результаты работы</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php' ?>