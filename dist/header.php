<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="font/css/all.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/aos.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <div class="header">
        <div class="container">
            <div class="header-top">
                <div class="row">
                    <div class="col-xl-6 col-12 col-md-12">
                        <div class="header-left">
                            <a href="index.php" class="logo">
                                <img src="images/logo.svg" alt="">
                            </a>
                            <a href="index.php">
                                <div class="tagline">
                                    <strong>ANSAR</strong>
                                    <p>Благотворительный фонд</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-6 col-12 col-md-12">
                        <div class="header-right">
                            <div class="phone">
                                <img src="images/phone.png" alt="">
                                <a href="">+7 (727) 317-16-98</a>
                            </div>
                            <div class="help-need">
                                <button type="button" class="btn" data-toggle="modal" data-target="#staticBackdrop">
                                    Нужна помощь
                                </button>
                                <div class="want-help">
                                    <a href="how-help.php">Хочу помочь</a>
                                </div>
                                <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false"
                                    tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="staticBackdropLabel">Способы помощи</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <strong aria-hidden="true">&times;</strong>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    Принципиальные подходы Благотворительного фонда «Добросердие» к
                                                    оказанию помощи в лечении и реабилитации детей с ДЦП
                                                </p>
                                                <h1>1.Общие принципы оказания помощи.</h1>
                                                <ul>
                                                    <li>1.1.Мы оказываем помощь в оплате лечения и реабилитации
                                                        детям-инвалидам с диагнозом детский церебральный паралич (ДЦП).
                                                    </li>
                                                    <li>1.2.Мы взаимодействуем с законным представителем ребенка, как
                                                        правило, родителем, действуя в интересах ребенка.</li>
                                                    <li>1.3.Решение об оказании помощи в лечении или реабилитации детей
                                                        с ДЦП принимается только после консультации с экспертом Фонда –
                                                        врачом-ортопедом.</li>
                                                    <li>1.4.Мы оплачиваем реабилитацию только в тех реабилитационных
                                                        центрах, которые указаны на сайте нашего фонда.</li>
                                                    <li>1.5.Мы стремимся оказать помощь максимальному количеству детей,
                                                        но из-за ограниченности наших ресурсов не можем помочь всем, кто
                                                        в этом нуждается.</li>
                                                    <li>1.6.Мы не можем гарантировать, что средства будут собраны и счет
                                                        оплачен к определенной дате.</li>
                                                    <li> 1.7.Мы оставляем за собой право отказать в помощи без
                                                        объяснения причин.</li>
                                                </ul>
                                                <h1>2.Кому, и на какие цели оказывается помощь.</h1>
                                                <ul>
                                                    <li>2.1.Мы оказываем помощь детям-инвалидам с ДЦП до 18 лет,
                                                        являющимся гражданами РФ, проживающими в регионах Москва и
                                                        Московская область, независимо от национальности, убеждений,
                                                        вероисповедания.</li>
                                                    <li>2.2.Мы помогаем семье ребенка- инвалида с ДЦП оплатить
                                                        дорогостоящее лечение или реабилитацию, в т.ч. покупку
                                                        технических средств реабилитации.</li>
                                                </ul>
                                                <h1>3.Условия предоставления помощи.</h1>
                                                <ul>
                                                    <li>3.1.Мы не переводим средств на счет законного представителя
                                                        ребенка, мы оплачиваем счет на лечение или реабилитацию за
                                                        законного представителя ребенка.</li>
                                                    <li>3.2.Мы считаем обязательным документальное подтверждение
                                                        законным представителем ребенка обстоятельств и фактов, на
                                                        которые есть ссылки в обращении в фонд.</li>
                                                </ul>
                                            </div>
                                            <div class="modal-footer">
                                                <a href="procedure.php">Принимаю</a>
                                                <button type="button" class="btn btn-close"
                                                    data-dismiss="modal">Отмена</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-links">
        <div class="container">
            <div class="row">
                <div class="col-xl-2">
                    <div class="header-link">
                        <a href="help-children.php">Помощь детям</a>
                    </div>
                </div>
                <div class="col-xl-2">
                    <div class="header-link header-link-medium">
                        <a href="project.php">Проекты</a>
                    </div>
                </div>
                <div class="col-xl-2">
                    <div class="header-link header-link-medium">
                        <a href="news.php">Новости</a>
                    </div>
                </div>
                <div class="col-xl-2">
                    <div class="header-link header-link-medium">
                        <a href="reports.php">Отчеты</a>
                    </div>
                </div>
                <div class="col-xl-2">
                    <div class="header-link header-link-medium">
                        <a href="fund.php">О фонде</a>
                    </div>
                </div>
                <div class="col-xl-2">
                    <div class="header-link header-link-right">
                        <a href="contacts.php">Контакты</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="burger-menu-content">
        <a href="#" class="burger-menu-btn">
            <strong class="burger-menu-lines"></strong>
        </a>
    </div>
    <div class="nav-panel-mobil">
        <div class="container">
            <nav class="navbar-expand-lg navbar-light">
                <ul class="navbar-nav nav-mobile navbar-mobile d-flex flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="about.php">Помощь детям</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="production.php">Проекты</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="solution.php">Новости</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="partners.php">Отчеты</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contacts.php">Получить помощь</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="letters.php">Контакты</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>