<?php include 'header.php' ?>
<main class="main">
        <div class="main__wrapper container">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная </a></li>
                <li class="breadcrumb-item active" aria-current="page">Новости</li>
            </ol>
        </nav>
        <div class="title">
            <h1>Новости</h1>
        </div>  
            
            <div class="news__row">
                <div class="news__card" data-aos-offset="200" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="100">
                    <a href="project-inner.php" class="news__card-img">
                        <img src="images/news-img3.jpg" alt="News image">
                    </a>
                    <a href="project-inner.php" class="news__card-title">Заменить ортезы и электроколяски на новые теперь можно без экспертизы</a>
                    <p class="news__card-content">Минтруд России сообщил об издании приказа, по которому упрощается порядок замены некоторых......</p>
                    <p class="news__card-date">Sep 8 2020</p>
                </div>

                <div class="news__card" data-aos-offset="200" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="100">
                    <a href="project-inner.php" class="news__card-img">
                        <img src="images/news-img2.jpg" alt="News image">
                    </a>
                    <a href="project-inner.php" class="news__card-title">Прошел 2-й и 3-й мастер-класс в рамках проекта внедрения программы «MOVE» в Елизаветинском саду</a>
                    <p class="news__card-content">В первой половине марта при поддержке фонда «Добросердие» прошли второй и третий мастер  ......</p>
                    <p class="news__card-date">Sep 8 2020</p>
                </div>

                <div class="news__card" data-aos-offset="200" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="100">
                    <a href="project-inner.php" class="news__card-img">
                        <img src="images/news-img1.jpg" alt="News image">
                    </a>
                    <a href="project-inner.php" class="news__card-title">Заменить ортезы и электроколяски на новые теперь можно без экспертизы</a>
                    <p class="news__card-content">Минтруд России сообщил об издании приказа, по которому упрощается порядок замены некоторых......</p>
                    <p class="news__card-date">Sep 8 2020</p>
                </div>

                <div class="news__card" data-aos-offset="200" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="100">
                    <a href="project-inner.php" class="news__card-img">
                        <img src="images/news-img3.jpg" alt="News image">
                    </a>
                    <a href="project-inner.php" class="news__card-title">Заменить ортезы и электроколяски на новые теперь можно без экспертизы</a>
                    <p class="news__card-content">Минтруд России сообщил об издании приказа, по которому упрощается порядок замены некоторых......</p>
                    <p class="news__card-date">Sep 8 2020</p>
                </div>

                <div class="news__card" data-aos-offset="200" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="100">
                    <a href="project-inner.php" class="news__card-img">
                        <img src="images/news-img2.jpg" alt="News image">
                    </a>
                    <a href="project-inner.php" class="news__card-title">Прошел 2-й и 3-й мастер-класс в рамках проекта внедрения программы «MOVE» в Елизаветинском саду</a>
                    <p class="news__card-content">В первой половине марта при поддержке фонда «Добросердие» прошли второй и третий мастер  ......</p>
                    <p class="news__card-date">Sep 8 2020</p>
                </div>

                <div class="news__card" data-aos-offset="200" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="100">
                    <a href="project-inner.php" class="news__card-img">
                        <img src="images/news-img1.jpg" alt="News image">
                    </a>
                    <a href="project-inner.php" class="news__card-title">Заменить ортезы и электроколяски на новые теперь можно без экспертизы</a>
                    <p class="news__card-content">Минтруд России сообщил об издании приказа, по которому упрощается порядок замены некоторых......</p>
                    <p class="news__card-date">Sep 8 2020</p>
                </div>

                <div class="news__card" data-aos-offset="200" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="100">
                    <a href="project-inner.php" class="news__card-img">
                        <img src="images/news-img3.jpg" alt="News image">
                    </a>
                    <a href="project-inner.php" class="news__card-title">Заменить ортезы и электроколяски на новые теперь можно без экспертизы</a>
                    <p class="news__card-content">Минтруд России сообщил об издании приказа, по которому упрощается порядок замены некоторых......</p>
                    <p class="news__card-date">Sep 8 2020</p>
                </div>

                <div class="news__card" data-aos-offset="200" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="100">
                    <a href="project-inner.php" class="news__card-img">
                        <img src="images/news-img2.jpg" alt="News image">
                    </a>
                    <a href="project-inner.php" class="news__card-title">Прошел 2-й и 3-й мастер-класс в рамках проекта внедрения программы «MOVE» в Елизаветинском саду</a>
                    <p class="news__card-content">В первой половине марта при поддержке фонда «Добросердие» прошли второй и третий мастер  ......</p>
                    <p class="news__card-date">Sep 8 2020</p>
                </div>

                <div class="news__card" data-aos-offset="200" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="100">
                    <a href="project-inner.php" class="news__card-img">
                        <img src="images/news-img1.jpg" alt="News image">
                    </a>
                    <a href="project-inner.php" class="news__card-title">Заменить ортезы и электроколяски на новые теперь можно без экспертизы</a>
                    <p class="news__card-content">Минтруд России сообщил об издании приказа, по которому упрощается порядок замены некоторых......</p>
                    <p class="news__card-date">Sep 8 2020</p>
                </div>
            </div>

            <div class="pagination">
                <ul>
                    <li class="prev">
                        <a href="#">
                            <svg viewBox="0 0 20 24" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.939341 10.9393C0.353554 11.5251 0.353554 12.4749 0.939341 13.0607L10.4853 22.6066C11.0711 23.1924 12.0208 23.1924 12.6066 22.6066C13.1924 22.0208 13.1924 21.0711 12.6066 20.4853L4.12132 12L12.6066 3.51472C13.1924 2.92893 13.1924 1.97919 12.6066 1.3934C12.0208 0.807611 11.0711 0.807611 10.4853 1.3934L0.939341 10.9393ZM20 10.5L2 10.5V13.5L20 13.5V10.5Z" fill="white"/>
                            </svg>
                        </a>
                    </li>
                    <li class="active">
                        <a href="#">1</a>
                    </li>
                    <li>
                        <a href="#">2</a>
                    </li>
                    <li>
                        <a href="#">3</a>
                    </li>
                    <li>
                        <a href="#">...10</a>
                    </li>
                    <li class="next">
                        <a href="#">
                            <svg viewBox="0 0 20 24" xmlns="http://www.w3.org/2000/svg">
                                <path d="M19.0607 13.0607C19.6464 12.4749 19.6464 11.5251 19.0607 10.9393L9.51472 1.3934C8.92893 0.807613 7.97919 0.807613 7.3934 1.3934C6.80761 1.97919 6.80761 2.92893 7.3934 3.51472L15.8787 12L7.3934 20.4853C6.80761 21.0711 6.80761 22.0208 7.3934 22.6066C7.97918 23.1924 8.92893 23.1924 9.51472 22.6066L19.0607 13.0607ZM-2.62268e-07 13.5L18 13.5L18 10.5L2.62268e-07 10.5L-2.62268e-07 13.5Z" fill="#46A85D"/>
                            </svg>                                
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </main>
<?php include 'footer.php' ?>